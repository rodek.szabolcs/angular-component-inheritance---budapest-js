import { Injectable } from '@angular/core';
import { name, finance, commerce, company, address, date } from 'faker';
import { BusinessClient } from './models/business-client';
import { Product } from './models/product';
import { Gender } from './models/gender';
import * as uuid from 'uuid';
@Injectable({
  providedIn: 'root',
})
export class DataService {
  private genders: Gender[] = ['female', 'male'];

  getClientList = (amount: number): BusinessClient[] => {
    return [...Array(amount)].map(() => {
      return {
        id: this.generateId(),
        firstName: name.firstName(),
        lastName: name.lastName(),
        joinDate: date.past(),
        country: address.country(),
        companyName: company.companyName(),
        accountName: finance.accountName(),
        iban: finance.iban(),
      };
    });
  };

  getProductList = (amount: number): Product[] =>
    [...Array(amount)].map(() => ({
      id: this.generateId(),
      name: commerce.productName(),
      color: commerce.color(),
      price: commerce.price(),
    }));

  private generateId(): string {
    return uuid.v4();
  }
}
