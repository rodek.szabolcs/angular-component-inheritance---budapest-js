import { Gender } from './gender';
import { Entity } from './entity';

export interface BusinessClient extends Entity {
  firstName: string;
  lastName: string;
  joinDate: Date;
  country: string;
  companyName: string;
  accountName: string;
  iban: string;
}
