export type ColumnType = 'string' | 'date' | 'currency';

export interface MaterialTableColumn<T> {
  name: string;
  type: ColumnType;
  cell: (data: T, key: string) => string;
}
