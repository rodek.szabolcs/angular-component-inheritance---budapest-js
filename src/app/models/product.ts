import { Entity } from './entity';

export interface Product extends Entity {
  name: string;
  color: string;
  price: string;
}
