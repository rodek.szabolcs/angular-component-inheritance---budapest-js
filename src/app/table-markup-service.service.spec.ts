import { TestBed } from '@angular/core/testing';

import { TableMarkupServiceService } from './table-markup-service.service';

describe('TableMarkupServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TableMarkupServiceService = TestBed.get(TableMarkupServiceService);
    expect(service).toBeTruthy();
  });
});
