import { Component, OnInit } from '@angular/core';
import { DataService } from './data.service';
import { BusinessClient, Product } from './models';

@Component({
  selector: 'aci-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  clients: BusinessClient[] = [];
  products: Product[] = []
  constructor(private service: DataService) {}

  ngOnInit() {
    this.clients = this.service.getClientList(10);
    this.products = this.service.getProductList(20);
  }

  onClientEdited(id: string) {
    console.log(`Client with id ${id} emitted for being edited.`);
  }

  onClientDeleted(id: string) {
    console.log(`Client with id ${id} emitted for being deleted.`);
  }

  onProductViewed(id: string) {
    console.log(`Product with id ${id} emitted for being viewed.`);
  }

  onProductEdited(id: string) {
    console.log(`Product with id ${id} emitted for being edited.`);
  }

  onProductDeleted(id: string) {
    console.log(`Product with id ${id} emitted for being deleted.`);
  }
}
