import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { MatTableModule } from '@angular/material/table';
import { MatCardModule } from '@angular/material/card';
import { BusinessClientsTableComponent } from './components/business-clients-table/business-clients-table.component';
import { SharedModule } from './shared/shared.module';
import { ProductTableComponent } from './components/product-table/product-table.component';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [AppComponent, BusinessClientsTableComponent, ProductTableComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    CommonModule,
    MatTableModule,
    MatCardModule,
    SharedModule,
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
