import { ColumnType } from '../../models';

export function DisplayedColumns(...columns: string[]) {
  const getName = (column: string): string => column.split(':')[0];
  const getType = (column: string): ColumnType => <ColumnType>column.split(':')[1] || 'string';

  return function(target: Function) {
    const columnTypes = columns.map(column => {
      const name = getName(column);
      const type: ColumnType = getType(column);
      return {
        name,
        type,
      };
    });

    target.prototype.columnTypes = columnTypes;
    target.prototype.displayedColumns = [...columns.map(getName), 'action'];
  };
}
