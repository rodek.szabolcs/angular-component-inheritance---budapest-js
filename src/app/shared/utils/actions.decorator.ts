import { MenuItem } from '../menu/menu.component';
import { Entity } from 'src/app/models';

export function Actions<S extends Entity>(...actionItems: { name: string; icon: string; eventName: string }[]) {
  return function<T extends { new (...args: any[]): {} }>(constructor: T) {
    return class extends constructor {
      constructor(...args: any[]) {
        super(...args);
      }
      actions: MenuItem<S>[] = actionItems.map(item => {
        if (!this[item.eventName] || !this[item.eventName].emit) {
          throw Error(`Exception: no @Output property called ${item.eventName} defined!`);
        }

        return <MenuItem<S>>{
          name: item.name,
          icon: item.icon,
          callback: (payload: S) => this[item.eventName].emit(payload.id),
        };
      });
    };
  };
}
