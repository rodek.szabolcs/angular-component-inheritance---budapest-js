import { Component, Input, Output, EventEmitter } from '@angular/core';

export interface MenuItem<T> {
  name: string;
  icon: string;
  callback: (payload: T) => void;
}

@Component({
  selector: 'aci-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
})
export class MenuComponent<T> {
  @Input()
  items: MenuItem<T>[];

  @Output()
  itemClicked: EventEmitter<(payload: T) => void> = new EventEmitter();

  emit(item: MenuItem<T>) {
    console.log(item);
    this.itemClicked.emit(item.callback);
  }
}
