import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'splitCamelCase'
})
export class SplitCamelCasePipe implements PipeTransform {
  transform(text: string): any {
    return text.replace(/[A-Z]/, (match: string) => ` ${match}`);
  }
}
