import { Component, OnInit, ChangeDetectionStrategy, Input } from '@angular/core';
import { Entity, MaterialTableColumn } from 'src/app/models';
import { MenuItem } from '../menu/menu.component';

@Component({
  selector: 'aci-mat-table-markup',
  templateUrl: './mat-table-markup.component.html',
  styleUrls: ['./mat-table-markup.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MatTableMarkupComponent<T extends Entity> {
  @Input()
  entities: T[];

  @Input()
  displayedColumns: string[];

  @Input()
  actions: MenuItem<T>[];

  @Input()
  columns: MaterialTableColumn<T>[];
}
