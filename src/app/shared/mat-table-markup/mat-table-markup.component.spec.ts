import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MatTableMarkupComponent } from './mat-table-markup.component';

describe('MatTableMarkupComponent', () => {
  let component: MatTableMarkupComponent;
  let fixture: ComponentFixture<MatTableMarkupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MatTableMarkupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MatTableMarkupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
