import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BaseMatTableComponent } from './base-mat-table.component';

describe('BaseMatTableComponent', () => {
  let component: BaseMatTableComponent;
  let fixture: ComponentFixture<BaseMatTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BaseMatTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BaseMatTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
