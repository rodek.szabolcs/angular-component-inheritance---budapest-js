import { Input, ViewContainerRef, ComponentFactoryResolver, Type, ComponentFactory, ComponentRef, OnDestroy, AfterViewInit } from '@angular/core';
import { Entity, ColumnType, MaterialTableColumn } from 'src/app/models';
import { MenuItem } from '../menu/menu.component';
import { MatTableMarkupComponent } from '../mat-table-markup/mat-table-markup.component';
import { TableMarkupService } from 'src/app/table-markup.service';

export abstract class BaseMatTableComponent<T extends Entity> implements OnDestroy {
  @Input()
  entities: T[];

  columnTypes: { name: string; type: ColumnType }[];
  columns: MaterialTableColumn<T>[];
  displayedColumns: string[];
  actions: MenuItem<T>[];

  constructor(protected dynamicComponentService: TableMarkupService, protected viewContainerRef: ViewContainerRef) {}

  ngOnDestroy() {
    this.destroyEmbeddedComponent();
  }

  onItemClicked(callback: (payload: T) => void, item: T): void {
    callback(item);
  }

  protected createEmbeddedComponent() {
    this.dynamicComponentService.appendComponentToHost(this.viewContainerRef);
    this.refreshEmbeddedComponent();
  }

  protected refreshEmbeddedComponent() {
    this.dynamicComponentService.addInputPropertiesToComponent(
      this.entities,
      this.actions,
      this.columns,
      this.displayedColumns,
    );
  }
  
  protected destroyEmbeddedComponent() {
    this.dynamicComponentService.destroyEmbeddedComponent();
  }

  protected columnFactory(name: string, type: ColumnType = 'string'): MaterialTableColumn<T> {
    return { name, type, cell: this.getCellRenderer(type) };
  }

  protected setColumns = () => {
    this.columns = this.columnTypes.map(({ name, type }) => this.columnFactory(name, type));
  }

  protected abstract getCellRenderer(type: ColumnType): (data: T, key: string) => string;
}
