import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MenuComponent } from './menu/menu.component';
import { MatMenuModule } from '@angular/material/menu';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { BaseMatTableComponent } from './base-mat-table/base-mat-table.component';
import { SplitCamelCasePipe } from './split-camel-case.pipe';
import { MatTableMarkupComponent } from './mat-table-markup/mat-table-markup.component';
import { MatTableModule } from '@angular/material/table';



@NgModule({
  declarations: [MenuComponent, SplitCamelCasePipe, MatTableMarkupComponent],
  imports: [
    CommonModule,
    MatTableModule,
    MatButtonModule,
    MatIconModule,
    MatMenuModule,
  ],
  entryComponents: [MatTableMarkupComponent],
  exports: [MenuComponent, SplitCamelCasePipe],
})
export class SharedModule { }
