import { Injectable, ComponentRef, ComponentFactoryResolver, ApplicationRef, Injector, ViewContainerRef } from '@angular/core';
import { SharedModule } from './shared/shared.module';
import { MatTableMarkupComponent } from './shared/mat-table-markup/mat-table-markup.component';
import { Entity, MaterialTableColumn } from './models';
import { BaseMatTableComponent } from './shared/base-mat-table/base-mat-table.component';
import { MenuItem } from './shared/menu/menu.component';

@Injectable({
  providedIn: SharedModule
})
export class TableMarkupService {
  componentRef: ComponentRef<MatTableMarkupComponent<Entity>>
  constructor(private componentFactoryResolver: ComponentFactoryResolver,
    private injector: Injector) { }

  appendComponentToHost(ref: ViewContainerRef) {
    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(MatTableMarkupComponent);
    this.componentRef = componentFactory.create(this.injector);
    ref.insert(this.componentRef.hostView);
  }

  addInputPropertiesToComponent(entities: Entity[], actions: MenuItem<Entity>[], columns: MaterialTableColumn<Entity>[], displayedColums: string[]) {
    this.componentRef.instance.entities = entities;
    this.componentRef.instance.actions = actions;
    this.componentRef.instance.columns = columns;
    this.componentRef.instance.displayedColumns = displayedColums;
  }

  destroyEmbeddedComponent() {
    this.componentRef.destroy();
  }
}
