import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  Output,
  EventEmitter,
  ViewContainerRef,
} from '@angular/core';
import { ColumnType, Product } from 'src/app/models';
import { CurrencyPipe } from '@angular/common';
import { BaseMatTableComponent } from 'src/app/shared/base-mat-table/base-mat-table.component';
import { DisplayedColumns, Actions } from 'src/app/shared/utils';
import { TableMarkupService } from 'src/app/table-markup.service';

@Actions<Product>(
  { name: 'View', icon: 'visibility', eventName: 'viewed' },
  { name: 'Edit', icon: 'edit', eventName: 'edited' },
  { name: 'Delete', icon: 'delete', eventName: 'deleted' },
)
@DisplayedColumns('name', 'color', 'price:currency')
@Component({
  selector: 'aci-product-table',
  template: '',
  providers: [CurrencyPipe],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProductTableComponent extends BaseMatTableComponent<Product> implements OnInit {
  @Output()
  viewed: EventEmitter<string> = new EventEmitter();

  @Output()
  edited: EventEmitter<string> = new EventEmitter();

  @Output()
  deleted: EventEmitter<string> = new EventEmitter();

  constructor(
    dynamicComponentService: TableMarkupService,
    viewContainerRef: ViewContainerRef,
    private currencyPipe: CurrencyPipe,
  ) {
    super(dynamicComponentService, viewContainerRef);
  }

  ngOnInit() {
    this.setColumns();
    this.createEmbeddedComponent();
  }

  protected getCellRenderer(type: ColumnType) {
    return (data: Product, key: string) => {
      switch (type) {
        case 'string': {
          return data[key];
        }

        case 'currency': {
          return this.currencyPipe.transform(data[key]);
        }

        default: {
          return data[key];
        }
      }
    };
  }
}
