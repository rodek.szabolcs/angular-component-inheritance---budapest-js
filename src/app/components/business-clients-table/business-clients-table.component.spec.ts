import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BusinessClientsTableComponent } from './business-clients-table.component';

describe('BusinessClientsTableComponent', () => {
  let component: BusinessClientsTableComponent;
  let fixture: ComponentFixture<BusinessClientsTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BusinessClientsTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BusinessClientsTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
