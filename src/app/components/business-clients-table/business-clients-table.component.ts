import { Component, OnInit, ChangeDetectionStrategy, Output, EventEmitter, ViewContainerRef, ComponentFactoryResolver, AfterViewChecked, AfterViewInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ColumnType, BusinessClient } from 'src/app/models';
import { BaseMatTableComponent } from 'src/app/shared/base-mat-table/base-mat-table.component';
import { DisplayedColumns, Actions } from 'src/app/shared/utils';
import { TableMarkupService } from 'src/app/table-markup.service';

@Actions<BusinessClient>(
  { name: 'Edit', icon: 'edit', eventName: 'edited' },
  { name: 'Delete', icon: 'delete', eventName: 'deleted' },
)
@DisplayedColumns('firstName', 'lastName', 'joinDate:date', 'country', 'companyName', 'accountName', 'iban')
@Component({
  selector: 'aci-business-clients-table',
  template: '',
  providers: [DatePipe],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BusinessClientsTableComponent extends BaseMatTableComponent<BusinessClient> implements OnInit {
  @Output()
  edited: EventEmitter<string> = new EventEmitter();

  @Output()
  deleted: EventEmitter<string> = new EventEmitter();

  constructor(viewContainerRef: ViewContainerRef, dynamicComponentService: TableMarkupService, private datePipe: DatePipe) {
    super(dynamicComponentService, viewContainerRef);
  }

  ngOnInit() {
    this.setColumns();
    this.createEmbeddedComponent();
  }

  protected getCellRenderer(type: ColumnType) {
    return (data: BusinessClient, key: string) => {
      switch (type) {
        case 'string': {
          return data[key];
        }

        case 'date': {
          return this.datePipe.transform(data[key], 'medium');
        }

        default: {
          return data[key];
        }
      }
    };
  }
}
